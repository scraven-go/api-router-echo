package apiRouterEcho

import (
	"github.com/labstack/echo"
	api "gitlab.com/scraven-go/sl-api"
	"net/http"
)

type Adapter struct {
	EchoInstance 	*echo.Echo
	ListenPort 		string
}

func (adapter *Adapter) Start() {

	e := adapter.EchoInstance
	e.Logger.Fatal(e.Start(":" + adapter.getPort()))
}

func (adapter *Adapter) LoadRoute(route api.Route) {
	handler := getHandler(route)

	switch route.Method {
	case "GET":
		adapter.EchoInstance.GET(route.Path, handler)
	case "POST":
		adapter.EchoInstance.POST(route.Path, handler)
	}
}

func getHandler(route api.Route) echo.HandlerFunc {
	return func(echoContext echo.Context) error {
		myRequest := api.NewRequest()

		// Copy URL parameters
		for _, pName := range echoContext.ParamNames() {
			myRequest.SetUrlParam(pName, echoContext.Param(pName))
		}

		// Copy body
		myRequest.SetBody(echoContext.Request().Body)

		myResponse := route.Handler(myRequest)

		return echoContext.String(http.StatusOK, myResponse.GetBody())
	}
}

// Optional Setting Defaults
func (adapter *Adapter) getPort() string {
	if adapter.ListenPort == "" {
		return "80"
	}

	return adapter.ListenPort
}